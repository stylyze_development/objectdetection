import csv
from matplotlib import pyplot as plt
from datetime import datetime
import logging
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
from PIL import Image 

image_file = "/home/paperspace/tpu/models/official/detection/projects/fashionpedia/image.jpg"
pickle_file = "/home/paperspace/tpu/models/official/detection/projects/fashionpedia/pickle_file"
mask_file = "/home/paperspace/tpu/models/official/detection/projects/fashionpedia//np_masks"
image_modified_file = "/home/paperspace/tpu/models/official/detection/projects/fashionpedia/image_b_1.jpg"
image_modified_2_file = "/home/paperspace/tpu/models/official/detection/projects/fashionpedia/image_b_2.jpg"

with open(pickle_file, "rb") as my_file:
    res = pickle.load(my_file)    

print(res[0]['classes'])    
for category in range(len(res[0]['classes'])):
    if res[0]['classes'][category] in [7,8]:
        position = category
        print(position)

top = int(res[0]["boxes"][position][0])
left = int(res[0]["boxes"][position][1])
bottom = int(res[0]["boxes"][position][2])
right = int(res[0]["boxes"][position][3])

im = Image.open(image_file)   
im1 = im.crop((left, top, right, bottom)) 
im1.save(image_modified_file)

with open(mask_file, "rb") as my_file:
    np_masks = pickle.load(my_file)
    
# print(len(np_masks))    

im3 = np.array(Image.open(image_file)) 
for h in range(np_masks[position].shape[0]):
    for w in range(np_masks[position].shape[1]):
        if np_masks[position][h][w] == 0:
            for i in range(3):
                im3[h][w][i] = 255
        else:
            continue
            
im3 = Image.fromarray(im3)           
im3.save(image_modified_2_file)