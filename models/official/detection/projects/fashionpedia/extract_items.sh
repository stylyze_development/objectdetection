export PYTHONPATH="$PYTHONPATH:/home/paperspace/tpu/models/official/detection"
export PYTHONPATH="$PYTHONPATH:/home/paperspace/tpu/models/official/efficientnet"
export PYTHONPATH="$PYTHONPATH:/home/paperspace/tpu/models/official"
export PYTHONPATH="$PYTHONPATH:/home/paperspace/tpu/models"

FILES=/home/paperspace/tpu/models/official/detection/projects/fashionpedia/images_modified_b/*
for f in $FILES
do
  echo "$f"
  cp "$f" /home/paperspace/tpu/models/official/detection/projects/fashionpedia/image.jpg
  part2=`basename "$f"`

  MODEL="attribute_mask_rcnn"
  IMAGE_SIZE=640
  CHECKPOINT_PATH="/home/paperspace/tpu/models/official/detection/projects/fashionpedia/checkpoint/fashionpedia-spinenet-143/model.ckpt"
  LABEL_MAP_FILE="/home/paperspace/tpu/models/official/detection/projects/fashionpedia/dataset/fashionpedia_label_map.csv"
  PARAMS_OVERRIDE="/home/paperspace/tpu/models/official/detection/projects/fashionpedia/configs/yaml/spinenet143_amrcnn.yaml"
  OUTPUT_HTML="./test.html"

  IMAGE_FILE_PATTERN="/home/paperspace/tpu/models/official/detection/projects/fashionpedia/image.jpg"
  
  python inference.py\
     --model="${MODEL?}"\
     --image_size=${IMAGE_SIZE?}\
     --checkpoint_path="${CHECKPOINT_PATH?}"\
     --label_map_file="${LABEL_MAP_FILE?}"\
     --image_file_pattern="${IMAGE_FILE_PATTERN?}"\
     --output_html="${OUTPUT_HTML?}"\
     --max_boxes_to_draw=15\
     --min_score_threshold=0.001\
     --params_override="${PARAMS_OVERRIDE?}"

  python extract_items.py

  cp /home/paperspace/tpu/models/official/detection/projects/fashionpedia/image_b_1.jpg /home/paperspace/tpu/models/official/detection/projects/fashionpedia/images_modified_b_1/$part2
  cp /home/paperspace/tpu/models/official/detection/projects/fashionpedia/image_b_2.jpg /home/paperspace/tpu/models/official/detection/projects/fashionpedia/images_modified_b_2/$part2

done